package box2c

import "core:c"

/// @file
/// Constants used by box2d.
/// box2d uses meters-kilograms-seconds (MKS) units. Angles are always in radians unless
/// degrees are indicated.
/// Some values can be overridden by using a compiler definition.
/// Other values cannot be modified without causing stability and/or performance problems.
///	Although most of these are not user configurable, it can be interesting for a user to see
///	these to understand the tuning values Box2D uses.

// ODIN: NOTE Configuring in Odin can't be done with checking for already existing defines.
// Instead, check below for constants using #config (these will be the same as in user_contstants.h)
// You can set these while compiling your program with -config.
// NOTE!! You must also recompile box2c, with the _exact_ same defines, or all hell will break loose!
// If you need to modify the rest of the defines, then you should know enough to figure that out on your own.

/// box2d bases all length units on meters, but you may need different units for your game.
/// You can override this value to use different units.
lengthUnitsPerMeter: f32 : #config(b2ConfigLengthUnitsPerMeter, 1.0)

/// https://en.wikipedia.org/wiki/Pi
pi :: 3.14159265359

/// This is used to fatten AABBs in the dynamic tree. This allows proxies
/// to move by a small amount without triggering a tree adjustment.
/// This is in meters.
/// @warning modifying this can have a significant impact on performance
aabbMargin :: (0.1 * lengthUnitsPerMeter)

/// A small length used as a collision and constraint tolerance. Usually it is
/// chosen to be numerically significant, but visually insignificant. In meters.
/// @warning modifying this can have a significant impact on stability
linearSlop :: (0.005 * lengthUnitsPerMeter)

/// A small angle used as a collision and constraint tolerance. Usually it is
/// chosen to be numerically significant, but visually insignificant.
/// @warning modifying this can have a significant impact on stability
angularSlop :: (2.0 / 180.0 * pi)

/// The maximum number of vertices on a convex polygon. Changing this affects performance even if you
///	don't use more vertices.
maxPolygonVertices :: #config(b2ConfigMaxPolygonVertices, 8)

/// Maximum number of simultaneous worlds that can be allocated
maxWorlds :: 128

/// The maximum linear translation of a body per step. This limit is very large and is used
/// to prevent numerical problems. You shouldn't need to adjust this. Meters.
/// @warning modifying this can have a significant impact on stability
maxTranslation :: (4.0 * lengthUnitsPerMeter)

/// The maximum angular velocity of a body. This limit is very large and is used
/// to prevent numerical problems. You shouldn't need to adjust this.
/// @warning modifying this can have a significant impact on stability
maxRotation :: (0.5 * pi)

/// @warning modifying this can have a significant impact on performance and stability
speculativeDistance :: (4.0 * linearSlop)

/// The time that a body must be still before it will go to sleep. In seconds.
timeToSleep :: 0.5

/// A body cannot sleep if its linear velocity is above this tolerance. Meters per second.
linearSleepTolerance :: (0.01 * lengthUnitsPerMeter)

/// A body cannot sleep if its angular velocity is above this tolerance. Radians per second.
angularSleepTolerance :: (2.0 / 180.0 * pi)

/// Used to detect bad values. Positions greater than about 16km will have precision
/// problems, so 100km as a limit should be fine in all cases.
huge :: (100000.0 * lengthUnitsPerMeter)

/// Maximum parallel workers. Used to size some static arrays.
maxWorkers :: 64

/// Solver graph coloring
graphColorCount :: 12

/// Version numbering scheme.
/// See http://en.wikipedia.org/wiki/Software_versioning
Version :: struct {
	///< significant changes
	major: c.int,

	///< incremental changes
	minor: c.int,

	///< bug fixes
	revision: c.int,
}

/// Current version.
version :: Version {3, 0, 0}
