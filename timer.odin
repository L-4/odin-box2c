package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// Timer for profiling. This has platform specific code and may not work on every platform.
when ODIN_OS == .Windows {
	Timer :: struct {
		start: i64,
	}
} else when ODIN_OS == .Linux || ODIN_OS == .Darwin {
	Timer :: struct {
		start_sec: c.ulonglong,
		start_usec: c.ulonglong,
	}
} else {
	Timer :: struct {
		dummy: c.int,
	}
}

@(link_prefix="b2")
foreign lib {
	CreateTimer :: proc() -> Timer ---
	GetTicks :: proc(timer: ^Timer) -> i64 ---
	GetMilliseconds :: proc(#by_ptr timer: Timer) -> f32 ---
	GetMillisecondsAndReset :: proc(timer: ^Timer) -> f32 ---
	SleepMilliseconds :: proc(milliseconds: f32) ---
}
