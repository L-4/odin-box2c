package box2c

JointType :: enum u32 {
	distance,
	motor,
	mouse,
	prismatic,
	revolute,
	weld,
	wheel,
}

/// Distance joint definition. This requires defining an anchor point on both
/// bodies and the non-zero distance of the distance joint. The definition uses
/// local anchor points so that the initial configuration can violate the
/// constraint slightly. This helps when saving and loading a game.
DistanceJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// The local anchor point relative to bodyA's origin.
	localAnchorA: Vec2,

	/// The local anchor point relative to bodyB's origin.
	localAnchorB: Vec2,

	/// The rest length of this joint. Clamped to a stable minimum value.
	length: f32,

	/// Minimum length. Clamped to a stable minimum value.
	minLength: f32,

	/// Maximum length. Must be greater than or equal to the minimum length.
	maxLength: f32,

	/// The linear stiffness hertz (cycles per second)
	hertz: f32,

	/// The linear damping ratio (non-dimensional)
	dampingRatio: f32,

	/// Set this flag to true if the attached bodies should collide.
	collideConnected: bool,

}

/// Use this to initialize your joint definition
defaultDistanceJointDef :: DistanceJointDef {
	bodyIdA          = NULL_BODY_ID,
	bodyIdB          = NULL_BODY_ID,
	localAnchorA     = {0.0, 0.0},
	localAnchorB     = {0.0, 0.0},
	length           = 1.0,
	minLength        = 0.0,
	maxLength        = huge,
	hertz            = 0.0,
	dampingRatio     = 0.0,
	collideConnected = false,
};

/// A motor joint is used to control the relative motion
/// between two bodies. A typical usage is to control the movement
/// of a dynamic body with respect to the ground.
MotorJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// Position of bodyB minus the position of bodyA, in bodyA's frame, in meters.
	linearOffset: Vec2,

	/// The bodyB angle minus bodyA angle in radians.
	angularOffset: f32,

	/// The maximum motor force in N.
	maxForce: f32,

	/// The maximum motor torque in N-m.
	maxTorque: f32,

	/// Position correction factor in the range [0,1].
	correctionFactor: f32,
}

/// Use this to initialize your joint definition
defaultMotorJointDef :: MotorJointDef {
	bodyIdA          = NULL_BODY_ID,
	bodyIdB          = NULL_BODY_ID,
	linearOffset     = {0.0, 0.0},
	angularOffset    = 0.0,
	maxForce         = 1.0,
	maxTorque        = 1.0,
	correctionFactor = 0.3,
};

/// A mouse joint is used to make a point on a body track a
/// specified world point. This a soft constraint with a maximum
/// force. This allows the constraint to stretch without
/// applying huge forces.
MouseJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// The initial target point in world space
	target: Vec2,

	/// The maximum constraint force that can be exerted
	/// to move the candidate body. Usually you will express
	/// as some multiple of the weight (multiplier * mass * gravity).
	maxForce: f32,

	/// The linear stiffness in N/m
	stiffness: f32,

	/// The linear damping in N*s/m
	damping: f32,
}

/// Use this to initialize your joint definition
defaultMouseJointDef :: MouseJointDef {
	bodyIdA   = NULL_BODY_ID,
	bodyIdB   = NULL_BODY_ID,
	target    = {0.0, 0.0},
	maxForce  = 0.0,
	stiffness = 0.0,
	damping   = 0.0,
}

/// Prismatic joint definition. This requires defining a line of
/// motion using an axis and an anchor point. The definition uses local
/// anchor points and a local axis so that the initial configuration
/// can violate the constraint slightly. The joint translation is zero
/// when the local anchor points coincide in world space.
PrismaticJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// The local anchor point relative to bodyA's origin.
	localAnchorA: Vec2,

	/// The local anchor point relative to bodyB's origin.
	localAnchorB: Vec2,

	/// The local translation unit axis in bodyA.
	localAxisA: Vec2,

	/// The constrained angle between the bodies: bodyB_angle - bodyA_angle.
	referenceAngle: f32,

	/// Enable/disable the joint limit.
	enableLimit: bool,

	/// The lower translation limit, usually in meters.
	lowerTranslation: f32,

	/// The upper translation limit, usually in meters.
	upperTranslation: f32,

	/// Enable/disable the joint motor.
	enableMotor: bool,

	/// The maximum motor force, usually in N.
	maxMotorForce: f32,

	/// The desired motor speed in radians per second.
	motorSpeed: f32,

	/// Set this flag to true if the attached bodies should collide.
	collideConnected: bool,
}

/// Use this to initialize your joint definition
defaultPrismaticJointDef :: PrismaticJointDef {
	bodyIdA          = NULL_BODY_ID,
	bodyIdB          = NULL_BODY_ID,
	localAnchorA     = {0.0, 0.0},
	localAnchorB     = {0.0, 0.0},
	localAxisA       = {1.0, 0.0},
	referenceAngle   = 0.0,
	enableLimit      = false,
	lowerTranslation = 0.0,
	upperTranslation = 0.0,
	enableMotor      = false,
	maxMotorForce    = 0.0,
	motorSpeed       = 0.0,
	collideConnected = false,
}

/// Revolute joint definition. This requires defining an anchor point where the
/// bodies are joined. The definition uses local anchor points so that the
/// initial configuration can violate the constraint slightly. You also need to
/// specify the initial relative angle for joint limits. This helps when saving
/// and loading a game.
/// The local anchor points are measured from the body's origin
/// rather than the center of mass because:
/// 1. you might not know where the center of mass will be.
/// 2. if you add/remove shapes from a body and recompute the mass,
///    the joints will be broken.
RevoluteJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// The local anchor point relative to bodyA's origin.
	localAnchorA: Vec2,

	/// The local anchor point relative to bodyB's origin.
	localAnchorB: Vec2,

	/// The bodyB angle minus bodyA angle in the reference state (radians).
	/// This defines the zero angle for the joint limit.
	referenceAngle: f32,

	/// A flag to enable joint limits.
	enableLimit: bool,

	/// The lower angle for the joint limit (radians).
	lowerAngle: f32,

	/// The upper angle for the joint limit (radians).
	upperAngle: f32,

	/// A flag to enable the joint motor.
	enableMotor: bool,

	/// The maximum motor torque used to achieve the desired motor speed.
	/// Usually in N-m.
	maxMotorTorque: f32,

	/// The desired motor speed. Usually in radians per second.
	motorSpeed: f32,

	/// Scale the debug draw
	drawSize: f32,

	/// Set this flag to true if the attached bodies should collide.
	collideConnected: bool,
}

/// Use this to initialize your joint definition
defaultRevoluteJointDef :: RevoluteJointDef {
	bodyIdA          = NULL_BODY_ID,
	bodyIdB          = NULL_BODY_ID,
	localAnchorA     = {0.0, 0.0},
	localAnchorB     = {0.0, 0.0},
	referenceAngle   = 0.0,
	enableLimit      = false,
	lowerAngle       = 0.0,
	upperAngle       = 0.0,
	enableMotor      = false,
	maxMotorTorque   = 0.0,
	motorSpeed       = 0.0,
	drawSize         = 0.25,
	collideConnected = false,
}

/// A weld joint connect to bodies together rigidly. This constraint can be made soft to mimic
///	soft-body simulation.
/// @warning the approximate solver in Box2D cannot hold many bodies together rigidly
WeldJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// The local anchor point relative to bodyA's origin.
	localAnchorA: Vec2,

	/// The local anchor point relative to bodyB's origin.
	localAnchorB: Vec2,

	/// The bodyB angle minus bodyA angle in the reference state (radians).
	referenceAngle: f32,

	/// Linear stiffness expressed as hertz (oscillations per second). Use zero for maximum stiffness.
	linearHertz: f32,

	/// Angular stiffness as hertz (oscillations per second). Use zero for maximum stiffness.
	angularHertz: f32,

	/// Linear damping ratio, non-dimensional. Use 1 for critical damping.
	linearDampingRatio: f32,

	/// Linear damping ratio, non-dimensional. Use 1 for critical damping.
	angularDampingRatio: f32,

	/// Set this flag to true if the attached bodies should collide.
	collideConnected: bool,
}

/// Use this to initialize your joint definition
defaultWeldJointDef :: WeldJointDef {
	bodyIdA             = NULL_BODY_ID,
	bodyIdB             = NULL_BODY_ID,
	localAnchorA        = {0.0, 0.0},
	localAnchorB        = {0.0, 0.0},
	referenceAngle      = 0.0,
	linearHertz         = 0.0,
	angularHertz        = 0.0,
	linearDampingRatio  = 1.0,
	angularDampingRatio = 1.0,
	collideConnected    = false,
}

/// Wheel joint definition. This requires defining a line of
/// motion using an axis and an anchor point. The definition uses local
/// anchor points and a local axis so that the initial configuration
/// can violate the constraint slightly. The joint translation is zero
/// when the local anchor points coincide in world space. Using local
/// anchors and a local axis helps when saving and loading a game.
WheelJointDef :: struct {
	/// The first attached body.
	bodyIdA: BodyId,

	/// The second attached body.
	bodyIdB: BodyId,

	/// The local anchor point relative to bodyA's origin.
	localAnchorA: Vec2,

	/// The local anchor point relative to bodyB's origin.
	localAnchorB: Vec2,

	/// The local translation unit axis in bodyA.
	localAxisA: Vec2,

	/// Enable/disable the joint limit.
	enableLimit: bool,

	/// The lower translation limit, usually in meters.
	lowerTranslation: f32,

	/// The upper translation limit, usually in meters.
	upperTranslation: f32,

	/// Enable/disable the joint motor.
	enableMotor: bool,

	/// The maximum motor torque, usually in N-m.
	maxMotorTorque: f32,

	/// The desired motor speed in radians per second.
	motorSpeed: f32,

	/// The linear stiffness in N/m
	stiffness: f32,

	/// The linear damping in N*s/m
	damping: f32,

	/// Set this flag to true if the attached bodies should collide.
	collideConnected: bool,
}

/// Use this to initialize your joint definition
defaultWheelJointDef :: WheelJointDef {
	bodyIdA          = NULL_BODY_ID,
	bodyIdB          = NULL_BODY_ID,
	localAnchorA     = {0.0, 0.0},
	localAnchorB     = {0.0, 0.0},
	localAxisA       = {1.0, 0.0},
	enableLimit      = false,
	lowerTranslation = 0.0,
	upperTranslation = 0.0,
	enableMotor      = false,
	maxMotorTorque   = 0.0,
	motorSpeed       = 0.0,
	stiffness        = 0.0,
	damping          = 0.0,
	collideConnected = false,
};
