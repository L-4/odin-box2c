package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// This holds the mass data computed for a shape.
MassData :: struct {
	/// The mass of the shape, usually in kilograms.
	mass: f32,

	/// The position of the shape's centroid relative to the shape's origin.
	center: Vec2,

	/// The rotational inertia of the shape about the local origin.
	I: f32,
}

/// A solid circle
Circle :: struct {
	point: Vec2,
	radius: f32,
}

/// A solid capsule
Capsule :: struct {
	point1, point2: Vec2,
	radius: f32,
}

/// A solid convex polygon. It is assumed that the interior of the polygon is to
/// the left of each edge.
/// Polygons have a maximum number of vertices equal to b2_maxPolygonVertices.
/// In most cases you should not need many vertices for a convex polygon.
///	@warning DO NOT fill this out manually, instead use a helper function like
///	b2MakePolygon or b2MakeBox.
Polygon :: struct {
	vertices: [maxPolygonVertices]Vec2,
	normals: [maxPolygonVertices]Vec2,
	centroid: Vec2,
	radius: f32,
	count: i32,
}

/// A line segment with two-sided collision.
Segment :: struct {
	point1, point2: Vec2,
}

/// A smooth line segment with one-sided collision. Only collides on the right side.
/// Several of these are generated for a chain shape.
/// ghost1 -> point1 -> point2 -> ghost2
SmoothSegment :: struct {
	/// The tail ghost vertex
	ghost1: Vec2,

	/// The line segment
	segment: Segment,

	/// The head ghost vertex
	ghost2: Vec2,

	/// The owning chain shape index (internal usage only)
	chainIndex: i32,
}

@(link_prefix="b2")
foreign lib {
	/// Validate ray cast input data (NaN, etc)
	IsValidRay :: proc(#by_ptr input: RayCastInput) -> bool ---

	/// Make a convex polygon from a convex hull. This will assert if the hull is not valid.
	MakePolygon :: proc(#by_ptr hull: Hull, radius: f32) -> Polygon ---

	/// Make an offset convex polygon from a convex hull. This will assert if the hull is not valid.
	MakeOffsetPolygon :: proc(#by_ptr hull: Hull, radius: f32, transform: Transform) -> Polygon ---

	/// Make a square polygon, bypassing the need for a convex hull.
	MakeSquare :: proc(h: f32) -> Polygon ---

	/// Make a box (rectangle) polygon, bypassing the need for a convex hull.
	MakeBox :: proc(hx: f32, hy: f32) -> Polygon ---

	/// Make a rounded box, bypassing the need for a convex hull.
	MakeRoundedBox :: proc(hx: f32, hy: f32, radius: f32) -> Polygon ---

	/// Make an offset box, bypassing the need for a convex hull.
	MakeOffsetBox :: proc(hx: f32, hy: f32, center: Vec2, angle: f32) -> Polygon ---

	/// Transform a polygon. This is useful for transfering a shape from one body to another.
	TransformPolygon :: proc(transform: Transform, #by_ptr polygon: Polygon) -> Polygon ---

	/// Compute mass properties of a circle
	ComputeCircleMass :: proc(#by_ptr shape: Circle, density: f32) -> MassData ---

	/// Compute mass properties of a capsule
	ComputeCapsuleMass :: proc(#by_ptr shape: Capsule, density: f32) -> MassData ---

	/// Compute mass properties of a polygon
	ComputePolygonMass :: proc(#by_ptr shape: Polygon, density: f32) -> MassData ---

	/// Compute the bounding box of a transformed circle
	ComputeCircleAABB :: proc(#by_ptr shape: Circle, transform: Transform) -> AABB ---

	/// Compute the bounding box of a transformed capsule
	ComputeCapsuleAABB :: proc(#by_ptr shape: Capsule, transform: Transform) -> AABB ---

	/// Compute the bounding box of a transformed polygon
	ComputePolygonAABB :: proc(#by_ptr shape: Polygon, transform: Transform) -> AABB ---

	/// Compute the bounding box of a transformed line segment
	ComputeSegmentAABB :: proc(#by_ptr shape: Segment, transform: Transform) -> AABB ---

	/// Test a point for overlap with a circle in local space
	PointInCircle :: proc(point: Vec2, #by_ptr shape: Circle) -> bool ---

	/// Test a point for overlap with a capsule in local space
	PointInCapsule :: proc(point: Vec2, #by_ptr shape: Capsule) -> bool ---

	/// Test a point for overlap with a convex polygon in local space
	PointInPolygon :: proc(point: Vec2, #by_ptr shape: Polygon) -> bool ---

	/// Ray cast versus circle in shape local space. Initial overlap is treated as a miss.
	RayCastCircle :: proc(#by_ptr input: RayCastInput, #by_ptr shape: Circle) -> RayCastOutput ---

	/// Ray cast versus capsule in shape local space. Initial overlap is treated as a miss.
	RayCastCapsule :: proc(#by_ptr input: RayCastInput, #by_ptr shape: Capsule) -> RayCastOutput ---

	/// Ray cast versus segment in shape local space. Optionally treat the segment as one-sided with hits from
	/// the left side being treated as a miss.
	RayCastSegment :: proc(#by_ptr input: RayCastInput, #by_ptr shape: Segment, oneSided: bool) -> RayCastOutput ---

	/// Ray cast versus polygon in shape local space. Initial overlap is treated as a miss.
	RayCastPolygon :: proc(#by_ptr input: RayCastInput, #by_ptr shape: Polygon) -> RayCastOutput ---

	/// Shape cast versus a circle. Initial overlap is treated as a miss.
	ShapeCastCircle :: proc(#by_ptr input: ShapeCastInput, #by_ptr shape: Circle) -> RayCastOutput ---

	/// Shape cast versus a capsule. Initial overlap is treated as a miss.
	ShapeCastCapsule :: proc(#by_ptr input: ShapeCastInput, #by_ptr shape: Capsule) -> RayCastOutput ---

	/// Shape cast versus a line segment. Initial overlap is treated as a miss.
	ShapeCastSegment :: proc(#by_ptr input: ShapeCastInput, #by_ptr shape: Segment) -> RayCastOutput ---

	/// Shape cast versus a convex polygon. Initial overlap is treated as a miss.
	ShapeCastPolygon :: proc(#by_ptr input: ShapeCastInput, #by_ptr shape: Polygon) -> RayCastOutput ---
}
