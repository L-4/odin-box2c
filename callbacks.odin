package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// Prototype for a pre-solve callback.
/// This is called after a contact is updated. This allows you to inspect a
/// contact before it goes to the solver. If you are careful, you can modify the
/// contact manifold (e.g. disable contact).
/// Notes:
///	- this function must be thread-safe
///	- this is only called if the shape has enabled presolve events
/// - this is called only for awake dynamic bodies
/// - this is not called for sensors
/// - the supplied manifold has impulse values from the previous step
///	Return false if you want to disable the contact this step
PreSolveFcn :: #type proc "c" (shapeIdA: ShapeId, shapeIdB: ShapeId, manifold: ^Manifold, context_: rawptr) -> bool

@(link_prefix="b2")
foreign lib {
	/// Register the pre-solve callback. This is optional.
	World_SetPreSolveCallback :: proc(worldId: WorldId, fcn: PreSolveFcn, context_: rawptr) ---
}

/// Prototype callback for AABB queries.
/// See b2World_Query
/// Called for each shape found in the query AABB.
/// @return false to terminate the query.
QueryResultFcn :: #type proc "c" (shapeId: ShapeId, context_: rawptr)

/// Prototype callback for ray casts.
/// See b2World::RayCast
/// Called for each shape found in the query. You control how the ray cast
/// proceeds by returning a float:
/// return -1: ignore this shape and continue
/// return 0: terminate the ray cast
/// return fraction: clip the ray to this point
/// return 1: don't clip the ray and continue
/// @param shape the shape hit by the ray
/// @param point the point of initial intersection
/// @param normal the normal vector at the point of intersection
/// @param fraction the fraction along the ray at the point of intersection
/// @return -1 to filter, 0 to terminate, fraction to clip the ray for
/// closest hit, 1 to continue
RayResultFcn :: #type proc "c" (shapeId: ShapeId, point: Vec2, normal: Vec2, fraction: f32, context_: rawptr) -> f32

/// Use an instance of this structure and the callback below to get the closest hit.
RayResult :: struct {
	shapeId: ShapeId,
	point: Vec2,
	normal: Vec2,
	fraction: f32,
	hit: bool,
}

emptyRayResult :: RayResult {{-1, -1, 0}, {0.0, 0.0}, {0.0, 0.0}, 0.0, false}
