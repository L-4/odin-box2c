package box2c

import "core:c"

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// Prototype for user allocation function.
///	@param size the allocation size in bytes
///	@param alignment the required alignment, guaranteed to be a power of 2
AllocFcn :: #type proc "c" (size: u32, alignment: i32) -> rawptr

/// Prototype for user free function.
///	@param mem the memory previously allocated through `b2AllocFcn`
FreeFcn :: #type proc "c" (mem: rawptr)

@(link_prefix="b2")
foreign lib {
	/// This allows the user to override the allocation functions. These should be
	///	set during application startup.
	SetAllocator :: proc(allocFcn: AllocFcn, freeFcn: FreeFcn) ---

	/// Total bytes allocated by Box2D
	GetByteCount :: proc() -> u32 ---
}

/// Prototype for the user assert callback. Return 0 to skip the debugger break.
AssertFcn :: #type proc "c" (condition: cstring, fileName: cstring, linueNumber: c.int) -> c.int

@(link_prefix="b2")
foreign lib {
	/// Override the default assert callback.
	///	@param assertFcn a non-null assert callback
	SetAssertFcn :: proc(assertFcn: AssertFcn) ---
}
