package box2c

/// A 2D vector
/// This can be used to represent a point or free vector.
Vec2 :: [2]f32 // ODIN: Alteration: use array programming

/// 2D rotation
Rot :: struct {
	/// Sine and cosine
	s, c: f32,
}

/// A 2D rigid transform
Transform :: struct {
	p: Vec2,
	q: Rot,
}

/// A 2-by-2 Matrix
Mat22 :: struct {
	/// columns
	cx, cy: Vec2,
}

/// Axis-aligned bounding box
AABB :: struct {
	lowerBound: Vec2,
	upperBound: Vec2,
}

/// Low level ray-cast input data
RayCastInput :: struct {
	origin, translation: Vec2,
	maxFraction: f32,
}

/// Low level hape cast input in generic form
ShapeCastInput :: struct {
	points: [maxPolygonVertices]Vec2,
	count: i32,
	radius: f32,
	translation: Vec2,
	maxFraction: f32,
}

// ODIN: named differently in box2c - declared as typedef struct RayCastOutput { ... } CastOutput. I think that this was the intention.
/// Low level ray-cast or shape-cast output data
RayCastOutput :: struct {
	normal: Vec2,
	point: Vec2,
	fraction: f32,
	iterations: i32,
	hit: bool,
}

/// Task interface
/// This is prototype for a Box2D task. Your task system is expected to invoke the Box2D task with these arguments.
/// The task spans a range of the parallel-for: [startIndex, endIndex)
/// The thread index must correctly identify each thread in the user thread pool, expected in [0, workerCount)
/// The task context is the context pointer sent from Box2D when it is enqueued.
TaskCallback :: #type proc "c" (startIndex: i32, endIndex: i32, threadIndex: i32, taskContext: rawptr)

/// These functions can be provided to Box2D to invoke a task system. These are designed to work well with enkiTS.
/// Returns a pointer to the user's task object. May be nullptr.
EnqueueTaskCallback :: #type proc "c" (task: ^TaskCallback, itemCount: i32, minRange: i32, taskContext: rawptr, userContext: rawptr) -> rawptr

/// Finishes a user task object that wraps a Box2D task.
FinishTaskCallback :: #type proc "c" (userTask: rawptr, userContext: rawptr)

/// World definition used to create a simulation world. Must be initialized using b2DefaultWorldDef.
WorldDef :: struct {
	/// Gravity vector. Box2D has no up-vector defined.
	gravity: Vec2,

	/// Restitution velocity threshold, usually in m/s. Collisions above this
	/// speed have restitution applied (will bounce).
	restitutionThreshold: f32,

	/// This parameter controls how fast overlap is resolved and has units of meters per second
	contactPushoutVelocity: f32,

	/// Contact stiffness. Cycles per second.
	contactHertz: f32,

	/// Contact bounciness. Non-dimensional.
	contactDampingRatio: f32,

	/// Can bodies go to sleep to improve performance
	enableSleep: bool,

	/// Capacity for bodies. This may not be exceeded.
	bodyCapacity: i32,

	/// initial capacity for shapes
	shapeCapacity: i32,

	/// Capacity for contacts. This may not be exceeded.
	contactCapacity: i32,

	/// Capacity for joints
	jointCapacity: i32,

	/// Stack allocator capacity. This controls how much space box2d reserves for per-frame calculations.
	/// Larger worlds require more space. b2Counters can be used to determine a good capacity for your
	/// application.
	arenaAllocatorCapacity: i32,

	/// task system hookup
	workerCount: u32,

	/// function to spawn task
	enqueueTask: ^EnqueueTaskCallback,

	/// function to finish a task
	finishTask: ^FinishTaskCallback,

	/// User context that is provided to enqueueTask and finishTask
	userTaskContext: rawptr,
}

/// Use this to initialize your world definition
defaultWorldDef :: WorldDef {
	gravity                = {0.0, -10.0},
	restitutionThreshold   = 1.0 * lengthUnitsPerMeter,
	contactPushoutVelocity = 3.0 * lengthUnitsPerMeter,
	contactHertz           = 30.0,
	contactDampingRatio    = 1.0,
	enableSleep            = true,
	bodyCapacity           = 0,
	shapeCapacity          = 0,
	contactCapacity        = 0,
	jointCapacity          = 0,
	arenaAllocatorCapacity = 1024 * 1024,
	workerCount            = 0,
	enqueueTask            = nil,
	finishTask             = nil,
	userTaskContext        = nil,
}

/// The body type.
/// static: zero mass, zero velocity, may be manually moved
/// kinematic: zero mass, non-zero velocity set by user, moved by solver
/// dynamic: positive mass, non-zero velocity determined by forces, moved by solver
BodyType :: enum u32 {
	static = 0,
	kinematic = 1,
	dynamic_ = 2,
}

/// A body definition holds all the data needed to construct a rigid body.
/// You can safely re-use body definitions. Shapes are added to a body after construction.
BodyDef :: struct {
	/// The body type: static, kinematic, or dynamic.
	/// Note: if a dynamic body would have zero mass, the mass is set to one.
	type: BodyType,

	/// The world position of the body. Avoid creating bodies at the origin
	/// since this can lead to many overlapping shapes.
	position: Vec2,

	/// The world angle of the body in radians.
	angle: f32,

	/// The linear velocity of the body's origin in world co-ordinates.
	linearVelocity: Vec2,

	/// The angular velocity of the body.
	angularVelocity: f32,

	/// Linear damping is use to reduce the linear velocity. The damping parameter
	/// can be larger than 1.0f but the damping effect becomes sensitive to the
	/// time step when the damping parameter is large.
	linearDamping: f32,

	/// Angular damping is use to reduce the angular velocity. The damping parameter
	/// can be larger than 1.0f but the damping effect becomes sensitive to the
	/// time step when the damping parameter is large.
	angularDamping: f32,

	/// Scale the gravity applied to this body.
	gravityScale: f32,

	/// Use this to store application specific body data.
	userData: rawptr,

	/// Set this flag to false if this body should never fall asleep. Note that
	/// this increases CPU usage.
	enableSleep: bool,

	/// Is this body initially awake or sleeping?
	isAwake: bool,

	/// Should this body be prevented from rotating? Useful for characters.
	fixedRotation: bool,

	/// Does this body start out enabled?
	isEnabled: bool,
}

/// Use this to initialize your body definition
defaultBodyDef :: BodyDef {
	type            = .static,
	position        = {0.0, 0.0},
	angle           = 0.0,
	linearVelocity  = {0.0, 0.0},
	angularVelocity = 0.0,
	linearDamping   = 0.0,
	angularDamping  = 0.0,
	gravityScale    = 1.0,
	userData        = nil,
	enableSleep     = true,
	isAwake         = true,
	fixedRotation   = false,
	isEnabled       = true,
}

/// This holds contact filtering data.
Filter :: struct {
	/// The collision category bits. Normally you would just set one bit.
	categoryBits: u32,

	/// The collision mask bits. This states the categories that this
	/// shape would accept for collision.
	maskBits: u32,

	/// Collision groups allow a certain group of objects to never collide (negative)
	/// or always collide (positive). Zero means no collision group. Non-zero group
	/// filtering always wins against the mask bits.
	groupIndex: i32,
}

/// Use this to initialize your filter
defaultFilter :: Filter {0x00000001, 0xFFFFFFFF, 0}

/// This holds contact filtering data.
QueryFilter :: struct {
	/// The collision category bits. Normally you would just set one bit.
	categoryBits: u32,

	/// The collision mask bits. This states the categories that this
	/// shape would accept for collision.
	maskBits: u32,
}

/// Use this to initialize your query filter
defaultQueryFilter :: QueryFilter {0x00000001, 0xFFFFFFFF}

/// Shape type
ShapeType :: enum u32 {
	capsule,
	circle,
	polygon,
	segment,
	smoothSegment,
}

/// Used to create a shape
ShapeDef :: struct {
	/// Use this to store application specific shape data.
	userData: rawptr,

	/// The friction coefficient, usually in the range [0,1].
	friction: f32,

	/// The restitution (bounce) usually in the range [0,1].
	restitution: f32,

	/// The density, usually in kg/m^2.
	density: f32,

	/// Contact filtering data.
	filter: Filter,

	/// A sensor shape collects contact information but never generates a collision response.
	isSensor: bool,

	/// Enable sensor events for this shape. Only applies to kinematic and dynamic bodies. Ignored for sensors.
	enableSensorEvents: bool,

	/// Enable contact events for this shape. Only applies to kinematic and dynamic bodies. Ignored for sensors.
	enableContactEvents: bool,

	/// Enable pre-solve contact events for this shape. Only applies to dynamic bodies. These are expensive
	///	and must be carefully handled due to multi-threading. Ignored for sensors.
	enablePreSolveEvents: bool,

}

/// Use this to initialize your shape definition
defaultShapeDef :: ShapeDef {
	userData             = nil,
	friction             = 0.6,
	restitution          = 0.0,
	density              = 1.0,
	filter               = {0x00000001, 0xFFFFFFFF, 0},
	isSensor             = false,
	enableSensorEvents   = true,
	enableContactEvents  = true,
	enablePreSolveEvents = false,
}

/// Used to create a chain of edges. This is designed to eliminate ghost collisions with some limitations.
///	- DO NOT use chain shapes unless you understand the limitations. This is an advanced feature!
///	- chains are one-sided
///	- chains have no mass and should be used on static bodies
///	- the front side of the chain points the right of the point sequence
///	- chains are either a loop or open
/// - a chain must have at least 4 points
///	- the distance between any two points must be greater than b2_linearSlop
///	- a chain shape should not self intersect (this is not validated)
///	- an open chain shape has NO COLLISION on the first and final edge
///	- you may overlap two open chains on their first three and/or last three points to get smooth collision
///	- a chain shape creates multiple hidden shapes on the body
ChainDef :: struct {
	/// An array of at least 4 points. These are cloned and may be temporary.
	points: [^]Vec2,

	/// The point count, must be 4 or more.
	count: i32,

	/// Indicates a closed chain formed by connecting the first and last points
	loop: bool,

	/// Use this to store application specific shape data.
	userData: rawptr,

	/// The friction coefficient, usually in the range [0,1].
	friction: f32,

	/// The restitution (elasticity) usually in the range [0,1].
	restitution: f32,

	/// Contact filtering data.
	filter: Filter,
}

/// Use this to initialize your chain definition
defaultChainDef :: ChainDef {
	points      = nil,
	count       = 0,
	loop        = false,
	userData    = nil,
	friction    = 0.6,
	restitution = 0.0,
	filter      = {0x00000001, 0xFFFFFFFF, 0},
}

/// Profiling data. Times are in milliseconds.
Profile :: struct {
	step: f32,
	pairs: f32,
	collide: f32,
	solve: f32,
	buildIslands: f32,
	solveConstraints: f32,
	broadphase: f32,
	continuous: f32,
}

/// Use this to initialize your profile
emptyProfile :: Profile {}

/// Counters that give details of the simulation size
Counters :: struct {
	islandCount: i32,
	bodyCount: i32,
	contactCount: i32,
	jointCount: i32,
	proxyCount: i32,
	pairCount: i32,
	treeHeight: i32,
	stackCapacity: i32,
	stackUsed: i32,
	byteCount: i32,
	taskCount: i32,
	colorCounts: [graphColorCount + 1]i32,
}

/// Use this to initialize your counters
emptyCounters :: Counters {}
