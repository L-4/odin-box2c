package box2c

import "core:c"

/// This struct holds callbacks you can implement to draw a box2d world.
DebugDraw :: struct {
	/// Draw a closed polygon provided in CCW order.
	DrawPolygon: proc "c" (vertices: [^]Vec2, vertexCount: c.int, color: Color, context_: rawptr),

	/// Draw a solid closed polygon provided in CCW order.
	DrawSolidPolygon: proc "c" (vertices: [^]Vec2, vertexCount: c.int, color: Color, context_: rawptr),

	/// Draw a rounded polygon provided in CCW order.
	DrawRoundedPolygon: proc "c" (vertices: [^]Vec2, vertexCount: c.int, radius: f32, lineColor: Color, fillColor: Color, context_: rawptr),

	/// Draw a circle.
	DrawCircle: proc "c" (center: Vec2, radius: f32, color: Color, context_: rawptr),

	/// Draw a solid circle.
	DrawSolidCircle: proc "c" (center: Vec2, radius: f32, axis: Vec2, color: Color, context_: rawptr),

	/// Draw a capsule.
	DrawCapsule: proc "c" (p1: Vec2, p2: Vec2, radius: f32, color: Color, context_: rawptr),

	/// Draw a solid capsule.
	DrawSolidCapsule: proc "c" (p1: Vec2, p2: Vec2, radius: f32, color: Color, context_: rawptr),

	/// Draw a line segment.
	DrawSegment: proc "c" (p1: Vec2, p2: Vec2, color: Color, context_: rawptr),

	/// Draw a transform. Choose your own length scale.
	/// @param xf a transform.
	DrawTransform: proc "c" (xf: Transform, context_: rawptr),

	/// Draw a point.
	DrawPoint: proc "c" (p: Vec2, size: f32, color: Color, context_: rawptr),

	/// Draw a string.
	DrawString: proc "c" (p: Vec2, s: cstring, context_: rawptr),

	drawShapes: bool,
	drawJoints: bool,
	drawAABBs: bool,
	drawMass: bool,
	drawContacts: bool,
	drawGraphColors: bool,
	drawContactNormals: bool,
	drawContactImpulses: bool,
	drawFrictionImpulses: bool,
	context_: rawptr,
}
