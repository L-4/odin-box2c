package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

@(link_prefix="b2")
foreign lib {
	/// Utility to compute linear stiffness values from frequency and damping ratio
	LinearStiffness :: proc(stiffness: ^f32, damping: ^f32, frequencyHertz: f32, dampingRatio: f32, bodyA: BodyId,
									 bodyB: BodyId) ---

	/// Utility to compute angular stiffness values from frequency and damping ratio
	AngularStiffness :: proc(stiffness: ^f32, damping: ^f32, frequencyHertz: f32, dampingRatio: f32, bodyA: BodyId,
									  bodyB: BodyId) ---
}
