package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// A manifold point is a contact point belonging to a contact
/// manifold. It holds details related to the geometry and dynamics
/// of the contact points.
ManifoldPoint :: struct {
	/// world coordinates of contact point
	point: Vec2,

	/// body anchors used by solver internally
	anchorA, anchorB: Vec2,

	/// the separation of the contact point, negative if penetrating
	separation: f32,

	/// the non-penetration impulse
	normalImpulse: f32,

	/// the friction impulse
	tangentImpulse: f32,

	/// uniquely identifies a contact point between two shapes
	id: u16,

	/// did this contact point exist the previous step?
	persisted: bool,
}

/// Contact manifold convex shapes.
Manifold :: struct {
	points: [2]ManifoldPoint,
	normal: Vec2,
	pointCount: i32,
}

/// Use this to initialize your manifold
emptyManifold :: Manifold {}

@(link_prefix="b2")
foreign lib {
	/// Compute the collision manifold between two circles.
	CollideCircles :: proc(#by_ptr circleA: Circle, xfA: Transform, #by_ptr circleB: Circle, xfB: Transform) -> Manifold ---

	/// Compute the collision manifold between a capsule and circle
	CollideCapsuleAndCircle :: proc(#by_ptr capsuleA: Capsule, xfA: Transform, #by_ptr circleB: Circle,
												   xfB: Transform) -> Manifold ---

	/// Compute the collision manifold between an segment and a circle.
	CollideSegmentAndCircle :: proc(#by_ptr segmentA: Segment, xfA: Transform, #by_ptr circleB: Circle,
												   xfB: Transform) -> Manifold ---

	/// Compute the collision manifold between a polygon and a circle.
	CollidePolygonAndCircle :: proc(#by_ptr polygonA: Polygon, xfA: Transform, #by_ptr circleB: Circle,
												   xfB: Transform) -> Manifold ---

	/// Compute the collision manifold between a capsule and circle
	CollideCapsules :: proc(#by_ptr capsuleA: Capsule, xfA: Transform, #by_ptr capsuleB: Capsule, xfB: Transform,
										   cache: ^DistanceCache) -> Manifold ---

	/// Compute the collision manifold between an segment and a capsule.
	CollideSegmentAndCapsule :: proc(#by_ptr segmentA: Segment, xfA: Transform, #by_ptr capsuleB: Capsule,
													xfB: Transform, cache: ^DistanceCache) -> Manifold ---

	/// Compute the collision manifold between a polygon and capsule
	CollidePolygonAndCapsule :: proc(#by_ptr polygonA: Polygon, xfA: Transform, #by_ptr capsuleB: Capsule,
													xfB: Transform, cache: ^DistanceCache) -> Manifold ---

	/// Compute the collision manifold between two polygons.
	CollidePolygons :: proc(#by_ptr polyA: Polygon, xfA: Transform, #by_ptr polyB: Polygon, xfB: Transform,
										   cache: ^DistanceCache) -> Manifold ---

	/// Compute the collision manifold between an segment and a polygon.
	CollideSegmentAndPolygon :: proc(#by_ptr segmentA: Segment, xfA: Transform, #by_ptr polygonB: Polygon,
													xfB: Transform, cache: ^DistanceCache) -> Manifold ---

	/// Compute the collision manifold between a smooth segment and a circle.
	CollideSmoothSegmentAndCircle :: proc(#by_ptr smoothSegmentA: SmoothSegment, xfA: Transform,
														 #by_ptr circleB: Circle, xfB: Transform) -> Manifold ---

	/// Compute the collision manifold between an segment and a capsule.
	CollideSmoothSegmentAndCapsule :: proc(#by_ptr segmentA: SmoothSegment, xfA: Transform, #by_ptr capsuleB: Capsule,
														  xfB: Transform, cache: ^DistanceCache) -> Manifold ---

	/// Compute the collision manifold between a smooth segment and a rounded polygon.
	CollideSmoothSegmentAndPolygon :: proc(#by_ptr segmentA: SmoothSegment, xfA: Transform, #by_ptr polygonB: Polygon,
														  xfB: Transform, cache: ^DistanceCache) -> Manifold ---
}
