package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// A convex hull. Used to create convex polygons.
Hull :: struct {
	points: [maxPolygonVertices]Vec2,
	count: i32,
}

@(link_prefix="b2")
foreign lib {
	/// Compute the convex hull of a set of points. Returns an empty hull if it fails.
	/// Some failure cases:
	/// - all points very close together
	/// - all points on a line
	/// - less than 3 points
	/// - more than b2_maxPolygonVertices points
	/// This welds close points and removes collinear points.
	ComputeHull :: proc(points: [^]Vec2, count: i32) -> Hull ---

	/// This determines if a hull is valid. Checks for:
	/// - convexity
	/// - collinear points
	/// This is expensive and should not be called at runtime.
	ValidateHull :: proc(#by_ptr hull: Hull) -> bool ---
}
