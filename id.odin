package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// These ids serve as handles to internal Box2D objects. These should be considered opaque data and passed by value.
/// Include this header if you need the id definitions and not the whole Box2D API.

/// World identifier
WorldId :: struct {
	index: i16,
	revision: u16,
}

/// Body identitifier
BodyId :: struct {
	index: i32,
	world: i16,
	revision: u16,
}

/// References a shape instance
ShapeId :: struct {
	index: i32,
	world: i16,
	revision: u16,
}

/// References a joint instance
JointId :: struct {
	index: i32,
	world: i16,
	revision: u16,
}

/// References a chain instances
ChainId :: struct {
	index: i32,
	world: i16,
	revision: u16,
}

/// Macros is needed for constant definitions
NULL_BODY_ID :: BodyId {-1, -1, 0}

/// Use these to make your identifiers null
nullWorldId :: WorldId {-1, 0}
nullBodyId  :: NULL_BODY_ID
nullShapeId :: ShapeId {-1, -1, 0}
nullJointId :: JointId {-1, -1, 0}
nullChainId :: ChainId {-1, -1, 0}

/// Macro to determine if any id is null
IS_NULL :: #force_inline proc "contextless" (id: $T) -> bool { return id.index == -1 }

/// Macro to determine if any id is non-null
NON_NULL :: #force_inline proc "contextless" (id: $T) -> bool { return id.index != -1 }

// Compare two ids for equality. Doesn't work for b2WorldId.
ID_EQUALS :: proc "contextless" (id1, id2: $T) -> bool { return id1.index == id2.index && id1.world == id2.world && id1.revision == id2.revision }

@(link_prefix="b2")
foreign lib {
	/// World identifier validation. Provides validation for up to 64K allocations.
	World_IsValid :: proc(id: WorldId) -> bool ---

	/// Body identifier validation. Provides validation for up to 64K allocations.
	Body_IsValid :: proc(id: BodyId) -> bool ---

	/// Shape identifier validation. Provides validation for up to 64K allocations.
	Shape_IsValid :: proc(id: ShapeId) -> bool ---

	/// Chain identifier validation. Provides validation for up to 64K allocations.
	Chain_IsValid :: proc(id: ChainId) -> bool ---

	/// Joint identifier validation. Provides validation for up to 64K allocations.
	Joint_IsValid :: proc(id: JointId) -> bool ---
}
