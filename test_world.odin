// +private
package box2c

import "core:testing"

@(test)
HelloWorld :: proc(t: ^testing.T) {
	// Define the gravity vector.
	gravity := Vec2 {0, -10}

	// Construct a world object, which will hold and simulate the rigid bodies.
	worldDef := defaultWorldDef
	worldDef.gravity = gravity

	worldId := CreateWorld(worldDef)

	// Define the ground body.
	groundBodyDef := defaultBodyDef
	groundBodyDef.position = Vec2{0, -10}

	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBodyId := CreateBody(worldId, groundBodyDef)

	// Define the ground box shape. The extents are the half-widths of the box.
	groundBox := MakeBox(50, 10)

	// Add the box shape to the ground body.
	groundShapeDef := defaultShapeDef
	CreatePolygonShape(groundBodyId, groundShapeDef, groundBox)

	// Define the dynamic body. We set its position and call the body factory.
	bodyDef := defaultBodyDef
	bodyDef.type = .dynamic_
	bodyDef.position = {0, 4}
	bodyId := CreateBody(worldId, bodyDef)

	// Define another box shape for our dynamic body.
	dynamicBox := MakeBox(1, 1)

	// Define the dynamic body shape
	shapeDef := defaultShapeDef

	// Set the box density to be non-zero, so it will be dynamic.
	shapeDef.density = 1

	// Override the default friction.
	shapeDef.friction = 0.3

	// Add the shape to the body.
	CreatePolygonShape(bodyId, shapeDef, dynamicBox)

	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	timeStep := f32(1.0 / 60.0)
	velocityIterations :: 6
	relaxIterations :: 2

	position := Body_GetPosition(bodyId)
	angle := Body_GetAngle(bodyId)

	// This is our little game loop.
	for i in 0..<60 {
		// Instruct the world to perform a single step of simulation.
		// It is generally best to keep the time step and iterations fixed.
		World_Step(worldId, timeStep, velocityIterations, relaxIterations)

		// Now print the position and angle of the body.
		position = Body_GetPosition(bodyId)
		angle = Body_GetAngle(bodyId)

		// testing.logf(t, "%4.2f %4.2f %4.2f", position.x, position.y, angle)
	}

	// When the world destructor is called, all bodies and joints are freed. This can
	// create orphaned ids, so be careful about your world management.
	DestroyWorld(worldId)

	testing.expect(t, abs(position.x) < 0.01)
	testing.expect(t, abs(position.y - 1) < 0.01)
	testing.expect(t, abs(angle) < 0.01)
}

@(test)
EmptyWorld :: proc(t: ^testing.T) {
	worldId := CreateWorld(defaultWorldDef)
	testing.expect(t, World_IsValid(worldId))

	timeStep :: f32(1.0 / 60.0)
	velocityIterations :: 6
	relaxIterations:: 2

	for i in 0..<60 {
		World_Step(worldId, timeStep, velocityIterations, relaxIterations)
	}

	DestroyWorld(worldId)

	testing.expect_value(t, World_IsValid(worldId), false)
}

BODY_COUNT :: 10
@(test)
DestroyAllBodiesWorld :: proc(t: ^testing.T) {
	worldId := CreateWorld(defaultWorldDef)
	testing.expect(t, World_IsValid(worldId))

	count := 0
	creating := true

	bodyIds: [BODY_COUNT]BodyId
	bodyDef := defaultBodyDef
	bodyDef.type = .dynamic_
	square := MakeSquare(0.5)

	for i in 0..<2 * BODY_COUNT + 10 {
		if creating {
			if count < BODY_COUNT {
				bodyIds[count] = CreateBody(worldId, bodyDef)
				CreatePolygonShape(bodyIds[count], defaultShapeDef, square)
				count += 1
			} else {
				creating = false
			}
		} else if count > 0 {
			DestroyBody(bodyIds[count - 1])
			bodyIds[count - 1] = nullBodyId
			count -= 1
		}

		World_Step(worldId, f32(1.0 / 60.0), 6, 2)
	}

	counters := World_GetCounters(worldId)
	testing.expect_value(t, counters.bodyCount, 0)

	DestroyWorld(worldId)

	testing.expect_value(t, World_IsValid(worldId), false)
}
