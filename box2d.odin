package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/**
 * \defgroup WorldAPI Worlds
 * These functions allow you to create a simulation world. You can then add bodies and
 * joints to the world and run the simulation. You can get contact information to get contact points
 * and normals as well as events. You can query to world, checking for overlaps and casting rays or shapes.
 * There is also debugging information such as debug draw, timing information, and counters.
 * @{
 */

@(link_prefix="b2")
foreign lib {
	/// Create a world for rigid body simulation. This contains all the bodies, shapes, and constraints.
	CreateWorld :: proc(#by_ptr def: WorldDef) -> WorldId ---

	/// Destroy a world.
	DestroyWorld :: proc(worldId: WorldId) ---

	/// Take a time step. This performs collision detection, integration,
	/// and constraint solution.
		/// @param timeStep the amount of time to simulate, this should not vary.
	/// @param velocityIterations for the velocity constraint solver.
	/// @param relaxIterations for reducing constraint bounce solver.
	World_Step :: proc(worldId: WorldId, timeStep: f32, velocityIterations: i32, relaxIterations: i32) ---

	/// Call this to draw shapes and other debug draw data. This is intentionally non-const.
	World_Draw :: proc(worldId: WorldId, debugDraw: ^DebugDraw) ---

	/// Get sensor events for the current time step. The event data is transient. Do not store a reference to this data.
	World_GetSensorEvents :: proc(worldId: WorldId) -> SensorEvents ---

	/// Get contact events for this current time step. The event data is transient. Do not store a reference to this data.
	World_GetContactEvents :: proc(worldId: WorldId) -> ContactEvents ---

	/// Query the world for all shapes that potentially overlap the provided AABB.
	World_QueryAABB :: proc(worldId: WorldId, fcn: QueryResultFcn, aabb: AABB, filter: QueryFilter, context_: rawptr) ---

	/// Query the world for all shapes that overlap the provided circle.
	World_OverlapCircle :: proc(worldId: WorldId, fcn: QueryResultFcn, #by_ptr circle: Circle, transform: Transform,
										 filter: QueryFilter, context_: rawptr) ---

	/// Query the world for all shapes that overlap the provided capsule.
	World_OverlapCapsule :: proc(worldId: WorldId, fcn: QueryResultFcn, #by_ptr capsule: Capsule, transform: Transform,
										  filter: QueryFilter, context_: rawptr) ---

	/// Query the world for all shapes that overlap the provided polygon.
	World_OverlapPolygon :: proc(worldId: WorldId, fcn: QueryResultFcn, #by_ptr polygon: Polygon, transform: Transform,
										  filter: QueryFilter, context_: rawptr) ---

	/// Ray-cast the world for all shapes in the path of the ray. Your callback
	/// controls whether you get the closest point, any point, or n-points.
	/// The ray-cast ignores shapes that contain the starting point.
	/// @param callback a user implemented callback class.
	/// @param point1 the ray starting point
	/// @param point2 the ray ending point
	World_RayCast :: proc(worldId: WorldId, origin: Vec2, translation: Vec2, filter: QueryFilter, fcn: RayResultFcn,
								   context_: rawptr) ---

	/// Ray-cast closest hit. Convenience function. This is less general than b2World_RayCast and does not allow for custom filtering.
	World_RayCastClosest :: proc(worldId: WorldId, origin: Vec2, translation: Vec2, filter: QueryFilter) -> RayResult ---

	/// Cast a circle through the world. Similar to a ray-cast except that a circle is cast instead of a point.
	World_CircleCast :: proc(worldId: WorldId, #by_ptr circle: Circle, originTransform: Transform, translation: Vec2,
									  filter: QueryFilter, fcn: RayResultFcn, context_: rawptr) ---

	/// Cast a capsule through the world. Similar to a ray-cast except that a capsule is cast instead of a point.
	World_CapsuleCast :: proc(worldId: WorldId, #by_ptr capsule: Capsule, originTransform: Transform, translation: Vec2,
									   filter: QueryFilter, fcn: RayResultFcn, context_: rawptr) ---

	/// Cast a capsule through the world. Similar to a ray-cast except that a polygon is cast instead of a point.
	World_PolygonCast :: proc(worldId: WorldId, #by_ptr polygon: Polygon, originTransform: Transform, translation: Vec2,
									   filter: QueryFilter, fcn: RayResultFcn, context_: rawptr) ---

	/// Enable/disable sleep. Advanced feature for testing.
	World_EnableSleeping :: proc(worldId: WorldId, flag: bool) ---

	/// Enable/disable constraint warm starting. Advanced feature for testing.
	World_EnableWarmStarting :: proc(worldId: WorldId, flag: bool) ---

	/// Enable/disable continuous collision. Advanced feature for testing.
	World_EnableContinuous :: proc(worldId: WorldId, flag: bool) ---

	/// Adjust the restitution threshold. Advanced feature for testing.
	World_SetRestitutionThreshold :: proc(worldId: WorldId, value: f32) ---

	/// Adjust contact tuning parameters:
	/// - hertz is the contact stiffness (cycles per second)
	/// - damping ratio is the contact bounciness with 1 being critical damping (non-dimensional)
	/// - push velocity is the maximum contact constraint push out velocity (meters per second)
	///	Advanced feature
	World_SetContactTuning :: proc(worldId: WorldId, hertz: f32, dampingRatio: f32, pushVelocity: f32) ---

	/// Get the current profile
	World_GetProfile :: proc(worldId: WorldId) -> Profile ---

	/// Get counters and sizes
	World_GetCounters :: proc(worldId: WorldId) -> Counters ---

	/** @} */

	/**
	 * \defgroup BodyAPI Bodies
	 * This is the body API.
	 * @{
	 */

	/// Create a rigid body given a definition. No reference to the definition is retained.
	/// @warning This function is locked during callbacks.
	CreateBody :: proc(worldId: WorldId, #by_ptr def: BodyDef) -> BodyId ---

	/// Destroy a rigid body given an id.
	/// @warning This function is locked during callbacks.
	DestroyBody :: proc(bodyId: BodyId) ---

	/// Get the type of a body
	Body_GetType :: proc(bodyId: BodyId) -> BodyType ---

	/// Set the type of a body. This has a similar cost to re-creating the body.
	Body_SetType :: proc(bodyId: BodyId, type: BodyType) ---

	/// Get the user data stored in a body
	Body_GetUserData :: proc(bodyId: BodyId) -> rawptr ---

	/// Get the world position of a body. This is the location of the body origin.
	Body_GetPosition :: proc(bodyId: BodyId) -> Vec2 ---

	/// Get the world angle of a body in radians.
	Body_GetAngle :: proc(bodyId: BodyId) -> f32 ---

	/// Get the world transform of a body.
	Body_GetTransform :: proc(bodyId: BodyId) -> Transform ---

	/// Set the world transform of a body. This acts as a teleport and is fairly expensive.
	Body_SetTransform :: proc(bodyId: BodyId, position: Vec2, angle: f32) ---

	/// Get a local point on a body given a world point
	Body_GetLocalPoint :: proc(bodyId: BodyId, worldPoint: Vec2) -> Vec2 ---

	/// Get a world point on a body given a local point
	Body_GetWorldPoint :: proc(bodyId: BodyId, localPoint: Vec2) -> Vec2 ---

	/// Get a local vector on a body given a world vector
	Body_GetLocalVector :: proc(bodyId: BodyId, worldVector: Vec2) -> Vec2 ---

	/// Get a world vector on a body given a local vector
	Body_GetWorldVector :: proc(bodyId: BodyId, localVector: Vec2) -> Vec2 ---

	/// Get the linear velocity of a body's center of mass
	Body_GetLinearVelocity :: proc(bodyId: BodyId) -> Vec2 ---

	/// Get the angular velocity of a body in radians per second
	Body_GetAngularVelocity :: proc(bodyId: BodyId) -> f32 ---

	/// Set the linear velocity of a body
	Body_SetLinearVelocity :: proc(bodyId: BodyId, linearVelocity: Vec2) ---

	/// Set the angular velocity of a body in radians per second
	Body_SetAngularVelocity :: proc(bodyId: BodyId, angularVelocity: f32) ---

	/// Apply a force at a world point. If the force is not
	/// applied at the center of mass, it will generate a torque and
	/// affect the angular velocity. This wakes up the body.
	/// @param force the world force vector, usually in Newtons (N).
	/// @param point the world position of the point of application.
	/// @param wake also wake up the body
	Body_ApplyForce :: proc(bodyId: BodyId, force: Vec2, point: Vec2, wake: bool) ---

	/// Apply a force to the center of mass. This wakes up the body.
	/// @param force the world force vector, usually in Newtons (N).
	/// @param wake also wake up the body
	Body_ApplyForceToCenter :: proc(bodyId: BodyId, force: Vec2, wake: bool) ---

	/// Apply a torque. This affects the angular velocity
	/// without affecting the linear velocity of the center of mass.
	/// @param torque about the z-axis (out of the screen), usually in N-m.
	/// @param wake also wake up the body
	Body_ApplyTorque :: proc(bodyId: BodyId, torque: f32, wake: bool) ---

	/// Apply an impulse at a point. This immediately modifies the velocity.
	/// It also modifies the angular velocity if the point of application
	/// is not at the center of mass. This wakes up the body.
	/// @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
	/// @param point the world position of the point of application.
	/// @param wake also wake up the body
	Body_ApplyLinearImpulse :: proc(bodyId: BodyId, impulse: Vec2, point: Vec2, wake: bool) ---

	/// Apply an impulse to the center of mass. This immediately modifies the velocity.
	/// @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
	/// @param wake also wake up the body
	Body_ApplyLinearImpulseToCenter :: proc(bodyId: BodyId, impulse: Vec2, wake: bool) ---

	/// Apply an angular impulse.
	/// @param impulse the angular impulse in units of kg*m*m/s
	/// @param wake also wake up the body
	Body_ApplyAngularImpulse :: proc(bodyId: BodyId, impulse: f32, wake: bool) ---

	/// Get the mass of the body (kilograms)
	Body_GetMass :: proc(bodyId: BodyId) -> f32 ---

	/// Get the inertia tensor of the body. In 2D this is a single number. (kilograms * meters^2)
	Body_GetInertiaTensor :: proc(bodyId: BodyId) -> f32 ---

	/// Get the center of mass position of the body in local space.
	Body_GetLocalCenterOfMass :: proc(bodyId: BodyId) -> Vec2 ---

	/// Get the center of mass position of the body in world space.
	Body_GetWorldCenterOfMass :: proc(bodyId: BodyId) -> Vec2 ---

	/// Override the body's mass properties. Normally this is computed automatically using the
	///	shape geometry and density. This information is lost if a shape is added or removed or if the
	///	body type changes.
	Body_SetMassData :: proc(bodyId: BodyId, massData: MassData) ---

	/// Get the mass data for a body.
	Body_GetMassData :: proc(bodyId: BodyId) -> MassData ---

	/// Adjust the linear damping. Normally this is set in b2BodyDef before creation.
	Body_SetLinearDamping :: proc(bodyId: BodyId, linearDamping: f32) ---

	/// Get the current linear damping.
	Body_GetLinearDamping :: proc(bodyId: BodyId) -> f32 ---

	/// Adjust the angular damping. Normally this is set in b2BodyDef before creation.
	Body_SetAngularDamping :: proc(bodyId: BodyId, angularDamping: f32) ---

	/// Get the current angular damping.
	Body_GetAngularDamping :: proc(bodyId: BodyId) -> f32 ---

	/// Adjust the gravity scale. Normally this is set in b2BodyDef before creation.
	Body_SetGravityScale :: proc(bodyId: BodyId, gravityScale: f32) ---

	/// Get the current gravity scale.
	Body_GetGravityScale :: proc(bodyId: BodyId) -> f32 ---

	/// Is this body awake?
	Body_IsAwake :: proc(bodyId: BodyId) -> bool ---

	/// Wake a body from sleep. This wakes the entire island the body is touching.
	Body_Wake :: proc(bodyId: BodyId) ---

	/// Is this body enabled?
	Body_IsEnabled :: proc(bodyId: BodyId) -> bool ---

	/// Disable a body by removing it completely from the simulation
	Body_Disable :: proc(bodyId: BodyId) ---

	/// Enable a body by adding it to the simulation
	Body_Enable :: proc(bodyId: BodyId) ---

	/// Iterate over shapes on a body
	Body_GetFirstShape :: proc(bodyId: BodyId) -> ShapeId ---
	Body_GetNextShape :: proc(shapeId: ShapeId) -> ShapeId ---

	/// Get the maximum capacity required for retrieving all the touching contacts on a body
	Body_GetContactCapacity :: proc(bodyId: BodyId) -> i32 ---

	/// Get the touching contact data for a body
	Body_GetContactData :: proc(bodyId: BodyId, contactData: ^ContactData, capacity: i32) -> i32 ---

	/// Get the current world AABB that contains all the attached shapes. Note that this may not emcompass the body origin.
	///	If there are no shapes attached then the returned AABB is empty and centered on the body origin.
	Body_ComputeAABB :: proc(bodyId: BodyId) -> AABB ---

	/** @} */

	/**
	 * \defgroup ShapeAPI Shapes
	 * This is the shape API.
	 * @{
	 */

	/// Create a circle shape and attach it to a body. The shape defintion and geometry are fully cloned.
	/// Contacts are not created until the next time step.
	///	@return the shape id for accessing the shape
	CreateCircleShape :: proc(bodyId: BodyId, #by_ptr def: ShapeDef, #by_ptr circle: Circle) -> ShapeId ---

	/// Create a line segment shape and attach it to a body. The shape defintion and geometry are fully cloned.
	/// Contacts are not created until the next time step.
	///	@return the shape id for accessing the shape
	CreateSegmentShape :: proc(bodyId: BodyId, #by_ptr def: ShapeDef, #by_ptr segment: Segment) -> ShapeId ---

	/// Create a capsule shape and attach it to a body. The shape defintion and geometry are fully cloned.
	/// Contacts are not created until the next time step.
	///	@return the shape id for accessing the shape
	CreateCapsuleShape :: proc(bodyId: BodyId, #by_ptr def: ShapeDef, #by_ptr capsule: Capsule) -> ShapeId ---

	/// Create a polygon shape and attach it to a body. The shape defintion and geometry are fully cloned.
	/// Contacts are not created until the next time step.
	///	@return the shape id for accessing the shape
	CreatePolygonShape :: proc(bodyId: BodyId, #by_ptr def: ShapeDef, #by_ptr polygon: Polygon) -> ShapeId ---

	/// Destroy any shape type
	DestroyShape :: proc(shapeId: ShapeId) ---

	/// Create a chain shape
	///	@see b2ChainDef for details
	CreateChain :: proc(bodyId: BodyId, #by_ptr def: ChainDef) -> ChainId ---

	/// Destroy a chain shape
	DestroyChain :: proc(chainId: ChainId) ---

	/// Get the type of a shape.
	Shape_GetType :: proc(shapeId: ShapeId) -> ShapeType ---

	/// Get the body that a shape is attached to
	Shape_GetBody :: proc(shapeId: ShapeId) -> BodyId ---

	/// Get the type of a shape.
	Shape_IsSensor :: proc(shapeId: ShapeId) -> bool ---

	/// Get the user data for a shape. This is useful when you get a shape id
	///	from an event or query
	Shape_GetUserData :: proc(shapeId: ShapeId) -> rawptr ---

	/// Set the density on a shape. Normally this is specified in b2ShapeDef.
	///	This will recompute the mass properties on the parent body.
	Shape_SetDensity :: proc(shapeId: ShapeId, density: f32) ---

	/// Get the density on a shape.
	Shape_GetDensity :: proc(shapeId: ShapeId) -> f32 ---

	/// Set the friction on a shape. Normally this is specified in b2ShapeDef.
	Shape_SetFriction :: proc(shapeId: ShapeId, friction: f32) ---

	/// Get the friction on a shape.
	Shape_GetFriction :: proc(shapeId: ShapeId) -> f32 ---

	/// Set the restitution (bounciness) on a shape. Normally this is specified in b2ShapeDef.
	Shape_SetRestitution :: proc(shapeId: ShapeId, restitution: f32) ---

	/// Get the restitution on a shape.
	Shape_GetRestitution :: proc(shapeId: ShapeId) -> f32 ---

	/// Get the current filter
	Shape_GetFilter :: proc(shapeId: ShapeId) -> Filter ---

	/// Set the current filter. This is almost as expensive as recreating the shape.
	Shape_SetFilter :: proc(shapeId: ShapeId, filter: Filter) ---

	/// Test a point for overlap with a shape
	Shape_TestPoint :: proc(shapeId: ShapeId, point: Vec2) -> bool ---

	/// Access the circle geometry of a shape.
	Shape_GetCircle :: proc(shapeId: ShapeId) -> (const_circle: ^Circle) ---

	/// Access the line segment geometry of a shape.
	Shape_GetSegment :: proc(shapeId: ShapeId) -> (const_segment: ^Segment) ---

	/// Access the smooth line segment geometry of a shape. These come from chain shapes.
	Shape_GetSmoothSegment :: proc(shapeId: ShapeId) -> (const_smoothsegment: ^SmoothSegment) ---

	/// Access the capsule geometry of a shape.
	Shape_GetCapsule :: proc(shapeId: ShapeId) -> (const_capsule: ^Capsule) ---

	/// Access the convex polygon geometry of a shape.
	Shape_GetPolygon :: proc(shapeId: ShapeId) -> (const_polygon: ^Polygon) ---

	/// If the type is b2_smoothSegmentShape then you can get the parent chain id.
	/// If the shape is not a smooth segment then this will return b2_nullChainId.
	Shape_GetParentChain :: proc(shapeId: ShapeId) -> ChainId ---

	/// Set the friction of a chain. Normally this is set in b2ChainDef.
	Chain_SetFriction :: proc(chainId: ChainId, friction: f32) ---

	/// Set the restitution (bounciness) on a chain. Normally this is specified in b2ChainDef.
	Chain_SetRestitution :: proc(chainId: ChainId, restitution: f32) ---

	/// Get the maximum capacity required for retrieving all the touching contacts on a shape
	Shape_GetContactCapacity :: proc(shapeId: ShapeId) -> i32 ---

	/// Get the touching contact data for a shape. The provided shapeId will be either shapeIdA or shapeIdB on the contact data.
	Shape_GetContactData :: proc(shapeId: ShapeId, contactData: ^ContactData, capacity: i32) -> i32 ---

	/// Get the current world AABB
	Shape_GetAABB :: proc(shapeId: ShapeId) -> AABB ---

	/** @} */

	/**
	 * \defgroup JointAPI Joints
	 * This is the joint API.
	 * @{
	 */

	/// Create a distance joint
	///	@see b2DistanceJointDef for details
	CreateDistanceJoint :: proc(worldId: WorldId, #by_ptr def: DistanceJointDef) -> JointId ---

	/// Create a motor joint
	///	@see b2MotorJointDef for details
	CreateMotorJoint :: proc(worldId: WorldId, #by_ptr def: MotorJointDef) -> JointId ---

	/// Create a mouse joint
	///	@see b2MouseJointDef for details
	CreateMouseJoint :: proc(worldId: WorldId, #by_ptr def: MouseJointDef) -> JointId ---

	/// Create a prismatic (slider) joint
	///	@see b2PrismaticJointDef for details
	CreatePrismaticJoint :: proc(worldId: WorldId, #by_ptr def: PrismaticJointDef) -> JointId ---

	/// Create a revolute (hinge) joint
	///	@see b2RevoluteJointDef for details
	CreateRevoluteJoint :: proc(worldId: WorldId, #by_ptr def: RevoluteJointDef) -> JointId ---

	/// Create a weld joint
	///	@see b2WeldJointDef for details
	CreateWeldJoint :: proc(worldId: WorldId, #by_ptr def: WeldJointDef) -> JointId ---

	/// Create a wheel joint
	///	@see b2WheelJointDef for details
	CreateWheelJoint :: proc(worldId: WorldId, #by_ptr def: WheelJointDef) -> JointId ---

	/// Destroy any joint type
	DestroyJoint :: proc(jointId: JointId) ---

	/// Get the joint type
	Joint_GetType :: proc(jointId: JointId) -> JointType ---

		/// Get body A on a joint
	Joint_GetBodyA :: proc(jointId: JointId) -> BodyId ---

	/// Get body B on a joint
	Joint_GetBodyB :: proc(jointId: JointId) -> BodyId ---

	/// Get the constraint force on a distance joint
	DistanceJoint_GetConstraintForce :: proc(jointId: JointId, timeStep: f32) -> f32 ---

	/// Set the length parameters of a distance joint
	///	@see b2DistanceJointDef for details
	DistanceJoint_SetLength :: proc(jointId: JointId, length: f32, minLength: f32, maxLength: f32) ---

	/// Get the current length of a distance joint
	DistanceJoint_GetCurrentLength :: proc(jointId: JointId) -> f32 ---

	/// Adjust the softness of a distance joint
	///	@see b2DistanceJointDef for details
	DistanceJoint_SetTuning :: proc(jointId: JointId, hertz: f32, dampingRatio: f32) ---

	/// Set the linear offset target for a motor joint
	MotorJoint_SetLinearOffset :: proc(jointId: JointId, linearOffset: Vec2) ---

	/// Set the angular offset target for a motor joint in radians
	MotorJoint_SetAngularOffset :: proc(jointId: JointId, angularOffset: f32) ---

	/// Set the maximum force for a motor joint
	MotorJoint_SetMaxForce :: proc(jointId: JointId, maxForce: f32) ---

	/// Set the maximum torque for a motor joint
	MotorJoint_SetMaxTorque :: proc(jointId: JointId, maxTorque: f32) ---

	/// Set the correction factor for a motor joint
	MotorJoint_SetCorrectionFactor :: proc(jointId: JointId, correctionFactor: f32) ---

	/// Get the current constraint force for a motor joint
	MotorJoint_GetConstraintForce :: proc(jointId: JointId, inverseTimeStep: f32) -> Vec2 ---

	/// Get the current constraint torque for a motor joint
	MotorJoint_GetConstraintTorque :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/// Set the target for a mouse joint
	MouseJoint_SetTarget :: proc(jointId: JointId, target: Vec2) ---

	/// Enable/disable a prismatic joint limit
	PrismaticJoint_EnableLimit :: proc(jointId: JointId, enableLimit: bool) ---

	/// Enable/disable a prismatic joint motor
	PrismaticJoint_EnableMotor :: proc(jointId: JointId, enableMotor: bool) ---

	/// Set the motor speed for a prismatic joint
	PrismaticJoint_SetMotorSpeed :: proc(jointId: JointId, motorSpeed: f32) ---

	/// Get the current motor force for a prismatic joint
	PrismaticJoint_GetMotorForce :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/// Set the maximum force for a pristmatic joint motor
	PrismaticJoint_SetMaxMotorForce :: proc(jointId: JointId, force: f32) ---

	/// Get the current constraint force for a prismatic joint
	PrismaticJoint_GetConstraintForce :: proc(jointId: JointId, inverseTimeStep: f32) -> Vec2 ---

	/// Get the current constraint torque for a prismatic joint
	PrismaticJoint_GetConstraintTorque :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/// Enable/disable a revolute joint limit
	RevoluteJoint_EnableLimit :: proc(jointId: JointId, enableLimit: bool) ---

	/// Enable/disable a revolute joint motor
	RevoluteJoint_EnableMotor :: proc(jointId: JointId, enableMotor: bool) ---

	/// Set the motor speed for a revolute joint in radians per second
	RevoluteJoint_SetMotorSpeed :: proc(jointId: JointId, motorSpeed: f32) ---

	/// Get the current motor torque for a revolute joint
	RevoluteJoint_GetMotorTorque :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/// Set the maximum torque for a revolute joint motor
	RevoluteJoint_SetMaxMotorTorque :: proc(jointId: JointId, torque: f32) ---

	/// Get the current constraint force for a revolute joint
	RevoluteJoint_GetConstraintForce :: proc(jointId: JointId, inverseTimeStep: f32) -> Vec2 ---

	/// Get the current constraint torque for a revolute joint
	RevoluteJoint_GetConstraintTorque :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/// Set the wheel joint stiffness
	WheelJoint_SetStiffness :: proc(jointId: JointId, stiffness: f32) ---

	/// Set the wheel joint damping
	WheelJoint_SetDamping :: proc(jointId: JointId, damping: f32) ---

	/// Enable/disable the wheel joint limit
	WheelJoint_EnableLimit :: proc(jointId: JointId, enableLimit: bool) ---

	/// Enable/disable the wheel joint motor
	WheelJoint_EnableMotor :: proc(jointId: JointId, enableMotor: bool) ---

	/// Set the wheel joint motor speed in radians per second
	WheelJoint_SetMotorSpeed :: proc(jointId: JointId, motorSpeed: f32) ---

	/// Get the wheel joint current motor torque
	WheelJoint_GetMotorTorque :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/// Set the wheel joint maximum motor torque
	WheelJoint_SetMaxMotorTorque :: proc(jointId: JointId, torque: f32) ---

	/// Get the current wheel joint constraint force
	WheelJoint_GetConstraintForce :: proc(jointId: JointId, inverseTimeStep: f32) -> Vec2 ---

	/// Get the current wheel joint constraint torque
	WheelJoint_GetConstraintTorque :: proc(jointId: JointId, inverseTimeStep: f32) -> f32 ---

	/** @} */
}
