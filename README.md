# Odin Box2c

Idiomatic Odin bindings for [Box2c](https://github.com/erincatto/box2c) (aka Box2D 3.0, not to be confused with older versions which were written in C++!)

Note that Box2c is still in an Alpha stage, though the official page claims that it is ready for testing. (See link above for up to date status)

### About the bindings:
 - Cases where const pointers are passed to a function instead use #by_ptr to convey this.
 - `math.h` and `math_cpp.h` have been removed. Prefer `core:linalg`
	- `b2.Vec2` is defined as `[2]f32`, so Box2c vectors are compatible with `core:linalg`
 - Some minor changes have been applied: search `ODIN:` to find these.
 - The `test_world.c` is reimplemented in Odin. run `odin test .` to test that the library works.
 - Bindings are based on commit `f4a72f1`

### Building
 - Windows amd64 binaries are provided in the repository
 - I found that when building box2c, I had to change the build mode from Multithreaded DLL to just Multithreaded
 - Binaries should be named `box2c_<windows|linux|darwin>_<x64|arm64>.<a|lib>`
