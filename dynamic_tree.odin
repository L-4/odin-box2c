package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

defaultCategoryBits :: 0x00000001
defaultMaskBits :: 0xFFFFFFFF

/// A node in the dynamic tree. The user does not interact with this directly.
/// 16 + 16 + 8 + pad(8)
TreeNode :: struct {
	aabb: AABB, // 16

	// Category bits for collision filtering
	categoryBits: u32, // 4

	using _: struct #raw_union {
		parent: i32,
		next: i32,
	}, // 4

	child1: i32, // 4
	child2: i32, // 4

	// todo could be union with child index
	userData: i32, // 4

	// leaf = 0, free node = -1
	height: i16, // 2

	enlarged: bool, // 1

	pad: [9]u8,
}

/// A dynamic AABB tree broad-phase, inspired by Nathanael Presson's btDbvt.
/// A dynamic tree arranges data in a binary tree to accelerate
/// queries such as AABB queries and ray casts. Leaf nodes are proxies
/// with an AABB. These are used to hold a user collision object, such as a reference to a b2Shape.
/// Nodes are pooled and relocatable, so I use node indices rather than pointers.
///	The dynamic tree is made available for advanced users that would like to use it to organize
///	spatial game data besides rigid bodies.
DynamicTree :: struct {
	nodes: [^]TreeNode,

	root: i32,
	nodeCount: i32,
	nodeCapacity: i32,
	freeList: i32,
	proxyCount: i32,

	leafIndices: [^]i32,
	leafBoxes: [^]AABB,
	leafCenters: [^]Vec2,
	binIndices: [^]i32,
	rebuildCapacity: i32,
}

@(link_prefix="b2")
foreign lib {
	/// Constructing the tree initializes the node pool.
	DynamicTree_Create :: proc() -> DynamicTree ---

	/// Destroy the tree, freeing the node pool.
	DynamicTree_Destroy :: proc(tree: ^DynamicTree) ---

	/// Create a proxy. Provide a tight fitting AABB and a userData value.
	DynamicTree_CreateProxy :: proc(tree: ^DynamicTree, aabb: AABB, categoryBits: u32, userData: i32) -> i32 ---

	/// Destroy a proxy. This asserts if the id is invalid.
	DynamicTree_DestroyProxy :: proc(tree: ^DynamicTree, proxyId: i32) ---

	// Clone one tree to another, reusing storage in the outTree if possible
	DynamicTree_Clone :: proc(outTree: ^DynamicTree, #by_ptr inTree: DynamicTree) ---

	/// Move a proxy to a new AABB by removing and reinserting into the tree.
	DynamicTree_MoveProxy :: proc(tree: ^DynamicTree, proxyId: i32, aabb: AABB) ---

	/// Enlarge a proxy and enlarge ancestors as necessary.
	DynamicTree_EnlargeProxy :: proc(tree: ^DynamicTree, proxyId: i32, aabb: AABB) ---
}

/// This function receives proxies found in the AABB query.
/// @return true if the query should continue
TreeQueryCallbackFcn :: #type proc "c" (proxyId: i32, userData: i32, context_: rawptr) -> bool

@(link_prefix="b2")
foreign lib {
	/// Query an AABB for overlapping proxies. The callback class
	/// is called for each proxy that overlaps the supplied AABB.
	DynamicTree_QueryFiltered :: proc(#by_ptr tree: DynamicTree, aabb: AABB, maskBits: u32,
											   callback: TreeQueryCallbackFcn, context_: rawptr) ---

	/// Query an AABB for overlapping proxies. The callback class
	/// is called for each proxy that overlaps the supplied AABB.
	DynamicTree_Query :: proc(#by_ptr tree: DynamicTree, aabb: AABB, callback: TreeQueryCallbackFcn, context_: rawptr) ---
}

/// This function receives clipped raycast input for a proxy. The function
/// returns the new ray fraction.
/// - return a value of 0 to terminate the ray cast
/// - return a value less than input->maxFraction to clip the ray
/// - return a value of input->maxFraction to continue the ray cast without clipping
TreeRayCastCallbackFcn :: #type proc "c" (#by_ptr input: RayCastInput, proxyId: i32, userData: i32, context_: rawptr) -> f32


@(link_prefix="b2")
foreign lib {
	/// Ray-cast against the proxies in the tree. This relies on the callback
	/// to perform a exact ray-cast in the case were the proxy contains a shape.
	/// The callback also performs the any collision filtering. This has performance
	/// roughly equal to k * log(n), where k is the number of collisions and n is the
	/// number of proxies in the tree.
	/// @param input the ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).
	/// @param callback a callback class that is called for each proxy that is hit by the ray.
	DynamicTree_RayCast :: proc(#by_ptr tree: DynamicTree, #by_ptr input: RayCastInput, maskBits: u32,
										callback: TreeRayCastCallbackFcn, context_: rawptr) ---
}

/// This function receives clipped raycast input for a proxy. The function
/// returns the new ray fraction.
/// - return a value of 0 to terminate the ray cast
/// - return a value less than input->maxFraction to clip the ray
/// - return a value of input->maxFraction to continue the ray cast without clipping
TreeShapeCastCallbackFcn :: #type proc "c" (#by_ptr input: ShapeCastInput, proxyId: i32, userData: i32, context_: rawptr) -> f32

@(link_prefix="b2")
foreign lib {
	/// Ray-cast against the proxies in the tree. This relies on the callback
	/// to perform a exact ray-cast in the case were the proxy contains a shape.
	/// The callback also performs the any collision filtering. This has performance
	/// roughly equal to k * log(n), where k is the number of collisions and n is the
	/// number of proxies in the tree.
	/// @param input the ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).
	/// @param callback a callback class that is called for each proxy that is hit by the ray.
	DynamicTree_ShapeCast :: proc(#by_ptr tree: DynamicTree, #by_ptr input: ShapeCastInput, maskBits: u32,
										   callback: TreeShapeCastCallbackFcn, context_: rawptr) ---

	/// Validate this tree. For testing.
	DynamicTree_Validate :: proc(#by_ptr tree: DynamicTree) ---

	/// Compute the height of the binary tree in O(N) time. Should not be
	/// called often.
	DynamicTree_GetHeight :: proc(#by_ptr tree: DynamicTree) -> i32 ---

	/// Get the maximum balance of the tree. The balance is the difference in height of the two children of a node.
	DynamicTree_GetMaxBalance :: proc(#by_ptr tree: DynamicTree) -> i32 ---

	/// Get the ratio of the sum of the node areas to the root area.
	DynamicTree_GetAreaRatio :: proc(#by_ptr tree: DynamicTree) -> f32 ---

	/// Build an optimal tree. Very expensive. For testing.
	DynamicTree_RebuildBottomUp :: proc(tree: ^DynamicTree) ---

	/// Get the number of proxies created
	DynamicTree_GetProxyCount :: proc(#by_ptr tree: DynamicTree) -> i32 ---

	/// Rebuild the tree while retaining subtrees that haven't changed. Returns the number of boxes sorted.
	DynamicTree_Rebuild :: proc(tree: ^DynamicTree, fullBuild: bool) -> i32 ---

	/// Shift the world origin. Useful for large worlds.
	/// The shift formula is: position -= newOrigin
	/// @param newOrigin the new origin with respect to the old origin
	DynamicTree_ShiftOrigin :: proc(tree: ^DynamicTree, newOrigin: Vec2) ---
}

/// Get proxy user data
/// @return the proxy user data or 0 if the id is invalid
DynamicTree_GetUserData :: #force_inline proc "contextless" (tree: DynamicTree, proxyId: i32) -> i32 {
	return tree.nodes[proxyId].userData
}


// /// Get the AABB of a proxy
DynamicTree_GetAABB :: #force_inline proc "contextless" (tree: DynamicTree, proxyId: i32) -> AABB {
	return tree.nodes[proxyId].aabb
}
