package box2c

when      ODIN_OS == .Windows { when ODIN_ARCH == .amd64 { foreign import lib "box2c_windows_x64.lib" } else { foreign import lib "box2c_windows_arm64.lib" } }
else when ODIN_OS == .Linux   { when ODIN_ARCH == .amd64 { foreign import lib "box2c_linux_x64.a" }     else { foreign import lib "box2c_linux_arm64.a" } }
else when ODIN_OS == .Darwin  { when ODIN_ARCH == .amd64 { foreign import lib "box2c_darwin_x64.a" }    else { foreign import lib "box2c_darwin_arm64.a" } }

/// Result of computing the distance between two line segments
SegmentDistanceResult :: struct {
	closest1: Vec2,
	closest2: Vec2,
	fraction1: f32,
	fraction2: f32,
	distanceSquared: f32,
}

@(link_prefix="b2")
foreign lib {
	/// Compute the distance between two line segments, clamping at the end points if needed.
	SegmentDistance :: proc(p1: Vec2, q1: Vec2, p2: Vec2, q2: Vec2) -> SegmentDistanceResult ---
}

/// A distance proxy is used by the GJK algorithm. It encapsulates any shape.
DistanceProxy :: struct {
	vertices: [maxPolygonVertices]Vec2,
	count: i32,
	radius: f32,
}

/// Used to warm start b2Distance.
/// Set count to zero on first call.
DistanceCache :: struct {
	metric: f32, ///< length or area
	count: u16,
	indexA: [3]u8, ///< vertices on shape A
	indexB: [3]u8, ///< vertices on shape B
}

emptyDistanceCache :: DistanceCache {}

/// Input for b2Distance.
/// You have to option to use the shape radii
/// in the computation. Even
DistanceInput :: struct {
	proxyA: DistanceProxy,
	proxyB: DistanceProxy,
	transformA: Transform,
	transformB: Transform,
	useRadii: bool,
}

/// Output for b2Distance.
DistanceOutput :: struct {
	pointA: Vec2, ///< closest point on shapeA
	pointB: Vec2, ///< closest point on shapeB
	distance: f32,
	iterations: i32, ///< number of GJK iterations used
}

@(link_prefix="b2")
foreign lib {
	/// Compute the closest points between two shapes. Supports any combination of:
	/// b2Circle, b2Polygon, b2EdgeShape. The simplex cache is input/output.
	/// On the first call set b2SimplexCache.count to zero.
	ShapeDistance :: proc(cache: ^DistanceCache, #by_ptr input: DistanceInput) -> DistanceOutput ---
}

/// Input parameters for b2ShapeCast
ShapeCastPairInput :: struct {
	proxyA: DistanceProxy,
	proxyB: DistanceProxy,
	transformA: Transform,
	transformB: Transform,
	translationB: Vec2,
	maxFraction: f32,
}

@(link_prefix="b2")
foreign lib {
	/// Perform a linear shape cast of shape B moving and shape A fixed. Determines the hit point, normal, and translation fraction.
	/// @returns true if hit, false if there is no hit or an initial overlap
	ShapeCast :: proc(#by_ptr input: ShapeCastPairInput) -> RayCastOutput ---

	/// Make a proxy for use in GJK and related functions.
	MakeProxy ::proc(vertices: [^]Vec2, count: i32, radius: f32) -> DistanceProxy ---
}

/// This describes the motion of a body/shape for TOI computation. Shapes are defined with respect to the body origin,
/// which may not coincide with the center of mass. However, to support dynamics we must interpolate the center of mass
/// position.
Sweep :: struct {
	/// local center of mass position
	localCenter: Vec2,

	/// center world positions
	c1, c2: Vec2,

	/// world angles
	a1, a2: f32,
}

@(link_prefix="b2")
foreign lib {
	GetSweepTransform :: proc(#by_ptr sweep: Sweep, time: f32) -> Transform ---
}

/// Input parameters for b2TimeOfImpact
TOIInput :: struct {
	proxyA: DistanceProxy,
	proxyB: DistanceProxy,
	sweepA: Sweep,
	sweepB: Sweep,

	// defines sweep interval [0, tMax]
	tMax: f32,
}

/// Describes the TOI output
TOIState :: enum u32 {
	Unknown,
	Failed,
	Overlapped,
	Hit,
	Separated,
}

/// Output parameters for b2TimeOfImpact.
TOIOutput :: struct {
	state: TOIState,
	t: f32,
}

@(link_prefix="b2")
foreign lib {
	/// Compute the upper bound on time before two shapes penetrate. Time is represented as
	/// a fraction between [0,tMax]. This uses a swept separating axis and may miss some intermediate,
	/// non-tunneling collisions. If you change the time interval, you should call this function
	/// again.
	TimeOfImpact :: proc(#by_ptr input: TOIInput) -> TOIOutput ---
}
